create table users
(
    id        text not null primary key,
    name      text not null,
    email     text not null,
    pass_hash text not null,
    age       int  null
);
create table posts
(
    id         text      not null primary key,
    user_id    text      not null,
    content    text      not null,
    created_at timestamp not null
);
create table tags
(
    id   text not null primary key,
    name text not null
);
create table posts_2_tag
(
    post_id text not null,
    tag_id  text not null,
    primary key (post_id, tag_id)
);