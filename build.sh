#!/bin/sh
echo pulling source from repo
git pull origin master

echo building jar-file
mvn clean package

echo building docker image
sudo docker build -t social-network:latest .
