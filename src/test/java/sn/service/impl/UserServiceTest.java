package sn.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import sn.dao.impl.UserStorageFacadeImpl;
import sn.dao.model.UserModel;
import sn.ex.CustomException;
import sn.service.dto.UserDto;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class UserServiceTest {
    @InjectMocks
    UserService subj;

    @Mock
    UserStorageFacadeImpl userRepository;

    @Spy
    UserDto2ModelConverter userConverter;

    @Mock
    PasswordEncoderImpl passwordEncoder;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void insertUser() {
        UserDto userDto = new UserDto(
                null,
                "Name",
                "email",
                25
        );

        when(passwordEncoder.getHash("pass"))
                .thenReturn("hash");

        subj.insertUser(userDto, "pass");

        verify(userConverter, times(1))
                .convertTo(userDto);
        verify(passwordEncoder, times(1))
                .getHash("pass");
    }

    @Test
    void login_success() {
        String email = "email";
        String pass = "pass";
        String passHash = "hash";
        UserModel userModel =
                new UserModel("id", "name", email, passHash, null, 25);
        when(userRepository.getUserByEmail(email))
                .thenReturn(Optional.of(userModel));
        when(passwordEncoder.validate(pass, passHash))
                .thenReturn(true);
        UserDto userDto = subj.login(email, pass).getUser();
        assertEquals(email, userDto.getEmail());
    }

    @Test
    void login_fails_userNotFound() {
        String email = "email";
        String pass = "pass";
        when(userRepository.getUserByEmail(email))
                .thenReturn(Optional.empty());

        CustomException ex = assertThrows(
                CustomException.class,
                () -> subj.login(email, pass));
        assertEquals("No user found by email", ex.getMessage());
        verifyNoInteractions(passwordEncoder);
        verifyNoInteractions(userConverter);
    }

    @Test
    void login_fails_passwordMismatch() {
        String email = "email";
        String pass = "pass";
        String passHash = "hash";
        UserModel userModel =
                new UserModel("id", "name", email, passHash, null, 25);
        when(userRepository.getUserByEmail(email))
                .thenReturn(Optional.of(userModel));
        when(passwordEncoder.validate(pass, passHash))
                .thenReturn(false);

        CustomException ex = assertThrows(
                CustomException.class,
                () -> subj.login(email, pass));
        assertEquals("Pass mismatch", ex.getMessage());

        verify(passwordEncoder, times(1))
                .validate(pass, passHash);
        verifyNoInteractions(userConverter);
    }


    @Test
    void getAllUsers() {
        when(userRepository.getAllUsers())
                .thenReturn(List.of(
                        new UserModel(
                                "id",
                                "name",
                                "email",
                                "passHash",
                                null,
                                25))
                );
        List<UserDto> allUsers = subj.getAllUsers();

        assertEquals(1, allUsers.size());
        assertEquals("id", allUsers.get(0).getId());

    }
}