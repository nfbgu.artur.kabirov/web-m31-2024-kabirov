package sn.api;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.api.json.InsertUserRequest;
import sn.api.json.LoginUserRequest;
import sn.api.json.UserWithPostsJson;
import sn.ex.CustomException;
import sn.service.dto.UserDto;
import sn.service.impl.SessionUserService;
import sn.service.impl.UserService;
import sn.utils.Json;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final SessionUserService sessionUserService;
    private final Json json;

    @RequestMapping(method = RequestMethod.GET, value = "users")
    public String getAllUsers() {
        List<UserDto> allUsers = userService.getAllUsers();
        return json.to(allUsers);
    }

    @PostMapping("insert")
    public void insertUser(
            @RequestParam String name,
            @RequestParam String email,
            @RequestParam String pass,
            @RequestParam int age) {
        userService.insertUser(new UserDto(null, name, email, age), pass);
    }

    @PostMapping("insert2/{name}/{email}/{age}/{pass}")
    public void insertUser2(
            @PathVariable String name,
            @PathVariable String email,
            @PathVariable String pass,
            @PathVariable int age) {
        userService.insertUser(new UserDto(null, name, email, age), pass);
    }

    @PostMapping("insert3")
    public void insertUser2(@RequestBody @Valid InsertUserRequest userRequest) {
        userService.insertUser(new UserDto(
                        null,
                        userRequest.getName(),
                        userRequest.getEmail(),
                        userRequest.getAge()),
                userRequest.getPass());
    }

    @GetMapping("login")
    public ResponseEntity<?> login(@RequestBody LoginUserRequest loginRequest, HttpServletRequest request) {
        try {
            UserWithPostsJson user = userService.login(loginRequest.getEmail(), loginRequest.getPass());
            sessionUserService.login(request, user.getUser());
            return ResponseEntity.ok(user);

        } catch (CustomException exception) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(exception.getMessage());
        }
    }

    @GetMapping("logout")
    public void logout(HttpServletRequest request) {
        sessionUserService.logout(request);
    }

}
