package sn.api.json;

import lombok.Getter;

@Getter
public class LoginUserRequest {
    String email;
    String pass;
}
