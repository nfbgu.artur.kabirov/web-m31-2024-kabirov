package sn.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.service.dto.PostDto;
import sn.service.dto.UserDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserWithPostsJson {
    UserDto user;
    List<PostDto> posts;
}
