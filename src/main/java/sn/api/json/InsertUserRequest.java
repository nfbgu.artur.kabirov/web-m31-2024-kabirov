package sn.api.json;

import jakarta.validation.constraints.*;
import lombok.Getter;

@Getter
public class InsertUserRequest {
    @NotBlank
    String name;

    @NotBlank
    @Email
    String email;

    @DecimalMin(value = "0")
    @NotNull
    Integer age;

    @NotNull
    @Size(min = 8)
    String pass;
}
