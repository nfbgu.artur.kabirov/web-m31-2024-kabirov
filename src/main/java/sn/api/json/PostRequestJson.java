package sn.api.json;


import lombok.Data;

import java.util.Set;

@Data
public class PostRequestJson {
    String content;
    Set<String> tags;
}
