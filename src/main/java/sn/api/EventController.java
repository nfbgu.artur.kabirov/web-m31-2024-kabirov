package sn.api;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.dao.EventStorageFacade;

import java.util.concurrent.*;

@RestController
@RequiredArgsConstructor
@EnableScheduling
public class EventController {
    private final EventStorageFacade eventStorage;
    BlockingQueue<Long> queue = new ArrayBlockingQueue<>(10_000);
    Long sum = 0L;
    private final Object lock = new Object();

    @PostConstruct
    void init() {
/*        ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(4);
        executorService.scheduleAtFixedRate(this::doSmth, 1, 2, TimeUnit.SECONDS);
        executorService.submit(this::worker);*/

        Thread thread = new Thread(this::worker);
        thread.start();

        Thread scheduleThread = new Thread(() -> {
            while (true) {
                try {
                    doSmth();
                    Thread.sleep(2_000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        scheduleThread.start();
    }

    @GetMapping("event")
    @SneakyThrows
    ResponseEntity<?> countEvent() {
        queue.offer(1L);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    private void worker() {
        System.out.println("Worker started");

        while (true) {
            Long value = queue.take();
            synchronized (lock) {
                sum += value;
                if (sum >= 37) {
                    System.out.println("worker " + sum);
                    eventStorage.saveEvent(sum);
                    sum = 0L;
                }
            }
        }

    }

   // @Scheduled(fixedDelay = 2_000)
    private void doSmth() {
        synchronized (lock) {
            if (sum > 0) {
                System.out.println("scheduled " + sum);
                eventStorage.saveEvent(sum);
                sum = 0L;
            }
        }
    }
}
