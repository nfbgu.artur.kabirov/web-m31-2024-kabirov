package sn.api;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.api.json.PostRequestJson;
import sn.api.json.PostResponseJson;
import sn.service.dto.PostDto;
import sn.service.dto.UserDto;
import sn.service.impl.PostService;
import sn.service.impl.SessionUserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;
    private final SessionUserService sessionUserService;

    @PostMapping("insert")
    public ResponseEntity<?> insertPost(@RequestBody PostRequestJson postRequestJson, HttpServletRequest request) {
        Optional<UserDto> user = sessionUserService.getUserFromRequest(request);
        if (user.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }

        PostDto postDto = postService.insertPost(new PostDto(
                null,
                user.get().getId(),
                postRequestJson.getContent(),
                postRequestJson.getTags(),
                null
        ));
        return ResponseEntity.ok(new PostResponseJson(
                postDto.getId(),
                postDto.getUserId(),
                postDto.getContent(),
                postDto.getCreatedAt()
        ));
    }

    @GetMapping("posts")
    public ResponseEntity<?> getPosts(HttpServletRequest request) {
        Optional<UserDto> user = sessionUserService.getUserFromRequest(request);
        if (user.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }

        List<PostResponseJson> posts = postService.getPostsByUserId(user.get().getId()).stream()
                .map(p -> new PostResponseJson(
                        p.getId(),
                        p.getUserId(),
                        p.getContent(),
                        p.getCreatedAt()
                ))
                .collect(Collectors.toList());
        return ResponseEntity.ok(posts);
    }
}
