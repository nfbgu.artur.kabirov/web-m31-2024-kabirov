package sn.cli;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sn.ex.CustomException;
import sn.service.dto.UserDto;
import sn.service.impl.UserService;

import java.util.Scanner;

@RequiredArgsConstructor
@Service
public class CliController {
    private final UserService userService;

    public void main() {
        while (true) {
            String input = getString("Enter command: ");
            if (input.equals("exit")) {
                break;
            }
            if (input.equals("1")) {
                String name = getString("Enter name");
                String email = getString("enter email");
                String pass = getString("enter password");
                int age = getInt("Enter age");
                userService.insertUser(new UserDto(
                                null,
                                name,
                                email,
                                age
                        ),
                        pass);
                System.out.println("Success");
            }
            if (input.equals("2")) {
                System.out.println(userService.getAllUsers());
            }
            if (input.equals("3")) {
                String email = getString("enter email");
                String pass = getString("enter password");
                try {
                    UserDto userDto = userService.login(email, pass).getUser();
                    System.out.println(userDto);
                } catch (CustomException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

    }

    private String getString(String text) {
        System.out.println(text);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    private int getInt(String text) {
        System.out.println(text);
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
}
