package sn.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@RequiredArgsConstructor
public class JsonImpl implements Json {
    private final ObjectMapper om;

    @Override
    @SneakyThrows
    public String to(Object o) {
        return om.writeValueAsString(o);
    }

    @Override
    @SneakyThrows
    public <T> T from(String json, Class<T> cls) {
        return om.readValue(json, cls);
    }
}
