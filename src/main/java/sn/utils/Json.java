package sn.utils;

public interface Json {
    String to(Object o);

    <T> T from(String json, Class<T> cls);
}
