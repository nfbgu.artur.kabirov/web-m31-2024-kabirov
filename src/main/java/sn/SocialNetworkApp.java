package sn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;
import sn.cli.CliController;

@SpringBootApplication
public class SocialNetworkApp {
    public static void main(String[] args) {
        SpringApplication.run(SocialNetworkApp.class, args);

  /*      ApplicationContext context = new AnnotationConfigApplicationContext("sn");
        CliController cliController = context.getBean(CliController.class);
        cliController.main();*/
    }

}
