package sn.dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.dao.model.TagModel;

import java.util.Optional;

public interface TagRepo extends JpaRepository<TagModel, String> {
    Optional<TagModel> findByName(String name);

}
