package sn.dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.dao.model.UserModel;

import java.util.Optional;

public interface UserRepo extends JpaRepository<UserModel, String> {
    Optional<UserModel> findUserModelByEmail(String email);
}
