package sn.dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.dao.model.Event;

public interface EventRepo extends JpaRepository<Event, String> {
}
