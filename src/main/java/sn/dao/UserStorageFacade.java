package sn.dao;

import sn.dao.model.UserModel;

import java.util.List;
import java.util.Optional;

public interface UserStorageFacade {
    void insertUser(UserModel user);

    List<UserModel> getAllUsers();

    Optional<UserModel> getUserByEmail(String email);
}
