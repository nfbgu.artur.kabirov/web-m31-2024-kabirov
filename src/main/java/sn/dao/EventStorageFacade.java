package sn.dao;

public interface EventStorageFacade {
    void saveEvent(long value);
}
