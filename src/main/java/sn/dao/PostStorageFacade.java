package sn.dao;

import sn.dao.model.PostModel;

import java.util.List;

public interface PostStorageFacade {
    List<PostModel> findAllPostsByUserId(String userId);

    void insertPost(PostModel post);
}
