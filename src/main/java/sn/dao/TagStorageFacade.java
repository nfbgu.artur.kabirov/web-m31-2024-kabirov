package sn.dao;

import sn.dao.model.TagModel;

public interface TagStorageFacade {
    TagModel insertTag(String name);
}
