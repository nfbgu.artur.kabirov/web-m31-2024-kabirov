package sn.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.dao.repo.UserRepo;
import sn.dao.UserStorageFacade;
import sn.dao.model.UserModel;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserStorageFacadeImpl implements UserStorageFacade {
    private final UserRepo userRepo;

    @Override
    @Transactional
    public void insertUser(UserModel user) {
        userRepo.save(user);
    }

    @Override
    public List<UserModel> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public Optional<UserModel> getUserByEmail(String email) {
        return userRepo.findUserModelByEmail(email);
    }
}
