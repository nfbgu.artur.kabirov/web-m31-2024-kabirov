package sn.dao.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.dao.PostStorageFacade;
import sn.dao.model.PostModel;

import java.util.List;

@Service

public class PostStorageFacadeImpl implements PostStorageFacade {
    @PersistenceContext
    EntityManager em;

    @Override
    @SuppressWarnings("unchecked")
    public List<PostModel> findAllPostsByUserId(String userId) {
        Query nativeQuery = em.createNativeQuery("""
                select * from posts
                where user_id = ?1
                """, PostModel.class);
        nativeQuery.setParameter(1, userId);
        return (List<PostModel>) nativeQuery.getResultList();
    }

    @Override
    @Transactional
    public void insertPost(PostModel post) {
        em.persist(post);
    }
}
