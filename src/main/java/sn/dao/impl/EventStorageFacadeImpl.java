package sn.dao.impl;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import sn.dao.EventStorageFacade;
import sn.dao.model.Event;
import sn.dao.repo.EventRepo;

@Service
@RequiredArgsConstructor
public class EventStorageFacadeImpl implements EventStorageFacade {
    private final static String EVENT_ID = "default_event";
    private final EventRepo eventRepo;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    public void saveEvent(long value) {
        Thread.sleep(100);
        Exception ex = null;
        for (int i = 0; i < 100; i++) {
            try {
                save(value);
                break;
            } catch (Exception e) {
                ex = e;
            }
        }

        if (ex !=null) {
            throw new RuntimeException(ex);
        }
    }

    private void save(long value) {

        final Event event = eventRepo.findById(EVENT_ID)
                .orElseGet(() -> new Event(EVENT_ID, 0L));
        event.setCount(event.getCount() + value);
        eventRepo.save(event);
        eventRepo.flush();
    }
}
