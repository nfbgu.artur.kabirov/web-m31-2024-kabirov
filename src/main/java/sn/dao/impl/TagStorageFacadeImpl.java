package sn.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sn.dao.repo.TagRepo;
import sn.dao.TagStorageFacade;
import sn.dao.model.TagModel;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TagStorageFacadeImpl implements TagStorageFacade {
    private final TagRepo repo;

    @Override
    public TagModel insertTag(String name) {
        TagModel tagModel = repo.findByName(name)
                .orElseGet(() ->
                        new TagModel(UUID.randomUUID().toString(), name));
        return repo.save(tagModel);
    }
}
