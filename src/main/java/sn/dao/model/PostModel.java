package sn.dao.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "posts")
public class PostModel {
    @Id
    String id;

    @Column(name = "user_id")
    String userId;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false, insertable = false)
    UserModel user;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "posts_2_tag",
            joinColumns = {@JoinColumn(name = "post_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    Set<TagModel> tags;

    String content;

    @Column(name = "created_at")
    ZonedDateTime createdAt;

}
