package sn.dao.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class UserModel {
    @Id
    String id;

    @Column(name = "name")
    String name;

    @Column(name = "email")
    String email;

    @Column(name = "pass_hash")
    String passHash;

    @OneToMany(mappedBy = "user")
    List<PostModel> posts;

    @Column(name = "age")
    Integer age;
}
