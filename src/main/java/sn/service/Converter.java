package sn.service;

public interface Converter<T, R> {
    R convertTo(T t);

    T convertBack(R r);
}
