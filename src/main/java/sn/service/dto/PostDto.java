package sn.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
public class PostDto {
    String id;
    String userId;
    String content;
    Set<String> tags;
    String createdAt;
}
