package sn.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@AllArgsConstructor
public class UserDto {
    String id;
    String name;
    String email;
    Integer age;
}
