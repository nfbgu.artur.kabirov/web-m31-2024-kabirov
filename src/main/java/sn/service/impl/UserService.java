package sn.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sn.api.json.UserWithPostsJson;
import sn.dao.UserStorageFacade;
import sn.dao.model.PostModel;
import sn.dao.model.UserModel;
import sn.ex.CustomException;
import sn.service.Converter;
import sn.service.PasswordEncoder;
import sn.service.dto.PostDto;
import sn.service.dto.UserDto;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserStorageFacade userStorageFacade;
    private final Converter<UserDto, UserModel> userConverter;
    private final Converter<PostDto, PostModel> postConverter;
    private final PasswordEncoder passwordEncoder;

    public void insertUser(UserDto user, String pass) {
        UserModel userModel = userConverter.convertTo(user);
        String hash = passwordEncoder.getHash(pass);
        userModel.setPassHash(hash);
        userModel.setId(UUID.randomUUID().toString());
        userStorageFacade.insertUser(userModel);
    }

    public UserWithPostsJson login(String email, String pass) {
        UserModel user = userStorageFacade.getUserByEmail(email)
                .orElseThrow(() -> new CustomException("No user found by email"));
        String passHash = user.getPassHash();
        boolean isPasswordEquals = passwordEncoder.validate(pass, passHash);
        if (!isPasswordEquals) {
            throw new CustomException("Pass mismatch");
        }

        return new UserWithPostsJson(
                userConverter.convertBack(user),
                Optional.ofNullable(user.getPosts())
                        .map(p -> p.stream()
                                .map(postConverter::convertBack)
                                .toList())
                        .orElseGet(Collections::emptyList)
        );
    }

    public List<UserDto> getAllUsers() {
        List<UserModel> allUsers = userStorageFacade.getAllUsers();
        return allUsers.stream()
                .map(user -> userConverter.convertBack(user))
                .collect(Collectors.toList());
    }
}
