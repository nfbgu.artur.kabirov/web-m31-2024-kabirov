package sn.service.impl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import sn.dao.model.UserModel;
import sn.service.Converter;
import sn.service.dto.UserDto;

@Component
public class UserDto2ModelConverter implements Converter<UserDto, UserModel> {

    public UserModel convertTo(UserDto user) {
        return new UserModel(
                user.getId(),
                user.getName(),
                user.getEmail(),
                null,
                null,
                user.getAge()
        );
    }

    public UserDto convertBack(UserModel user) {
        return new UserDto(
                user.getId(),
                user.getName(),
                user.getEmail(),
                user.getAge()
        );
    }
}
