package sn.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sn.dao.PostStorageFacade;
import sn.dao.repo.TagRepo;
import sn.dao.model.PostModel;
import sn.dao.model.TagModel;
import sn.service.dto.PostDto;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostService {
    private final PostStorageFacade postStorageFacade;
    private final TagRepo repo;
    private final PostDto2ModelConverter converter;
    private final TimeManager timeManager;

    public PostDto insertPost(PostDto postDto) {
        PostModel postModel = converter.convertTo(postDto);
        postModel.setId(UUID.randomUUID().toString());
        postModel.setCreatedAt(timeManager.nowZ());

        Set<TagModel> tags = postDto.getTags().stream()
                .map(name ->
                        repo.findByName(name)
                                .orElseGet(() ->
                                        new TagModel(UUID.randomUUID().toString(), name)))
                .collect(Collectors.toSet());

        postModel.setTags(tags);

        postStorageFacade.insertPost(postModel);
        return converter.convertBack(postModel);
    }

    public List<PostDto> getPostsByUserId(String userId) {
        return postStorageFacade.findAllPostsByUserId(userId).stream()
                .map(converter::convertBack)
                .toList();
    }
}
