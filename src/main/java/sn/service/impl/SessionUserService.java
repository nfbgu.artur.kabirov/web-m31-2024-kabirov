package sn.service.impl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import sn.service.dto.UserDto;

import java.util.Optional;


@Component
public class SessionUserService {
    private final static String USER_SESSION_ATTRIBUTE = "user";

    public Optional<UserDto> getUserFromRequest(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(USER_SESSION_ATTRIBUTE);
        if (attribute == null) {
            return Optional.empty();
        }
        return Optional.of((UserDto) attribute);
    }

    public void login(HttpServletRequest request, UserDto user) {
        HttpSession session = request.getSession();
        session.setAttribute(USER_SESSION_ATTRIBUTE, user);
    }

    public void logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute(USER_SESSION_ATTRIBUTE, null);
    }
}
