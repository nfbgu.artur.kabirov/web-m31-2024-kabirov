package sn.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;
import sn.service.PasswordEncoder;

@Component
public class PasswordEncoderImpl implements PasswordEncoder {
    @Override
    public String getHash(String password) {
        return DigestUtils.md5Hex(password);
    }

    @Override
    public boolean validate(String password, String hash) {
        return DigestUtils.md5Hex(password).equals(hash);
    }
}
