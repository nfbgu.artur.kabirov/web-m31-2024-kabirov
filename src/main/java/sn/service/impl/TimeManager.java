package sn.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class TimeManager {

    ZonedDateTime nowZ() {
        return ZonedDateTime.now();
    }

}
