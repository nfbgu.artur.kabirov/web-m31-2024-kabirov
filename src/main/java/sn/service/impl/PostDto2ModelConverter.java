package sn.service.impl;

import org.springframework.stereotype.Component;
import sn.dao.model.PostModel;
import sn.dao.model.TagModel;
import sn.service.Converter;
import sn.service.dto.PostDto;

import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class PostDto2ModelConverter implements Converter<PostDto, PostModel> {
    @Override
    public PostModel convertTo(PostDto postDto) {
        return new PostModel(
                postDto.getId(),
                postDto.getUserId(),
                null,
                null,
                postDto.getContent(),
                null
        );
    }

    @Override
    public PostDto convertBack(PostModel postModel) {
        return new PostDto(
                postModel.getId(),
                postModel.getUserId(),
                postModel.getContent(),
                postModel.getTags().stream()
                        .map(TagModel::getName)
                        .collect(Collectors.toSet()),
                postModel.getCreatedAt().toString()
        );
    }
}
