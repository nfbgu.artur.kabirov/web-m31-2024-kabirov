package sn.service;

public interface PasswordEncoder {
    String getHash(String password);

    boolean validate(String password, String hash);
}
